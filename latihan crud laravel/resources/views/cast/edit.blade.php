@extends('layout.master')
@section('judul')
    Edit Genre
@endsection
@section('content')
<div>
    <h2>Tambah Data</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label>Nama Cast</label>
                <input type="text" class="form-control" value="{{$cast->nama}}" name="nama"  placeholder="Masukkan Nama Cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="integer" class="form-control" value="{{$cast->umur}}" name="umur"  placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea name="bio" class="form-control">{{$cast->bio}}</textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>

@endsection