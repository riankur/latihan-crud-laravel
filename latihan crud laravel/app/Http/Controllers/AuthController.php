<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata(){
        return view('halaman.register');
    }
    public function welcome(Request $request){
        $nama_awal = $request['nama_awal'];
        $nama_akhir = $request['nama_akhir'];

        return view('halaman.welcome', compact('nama_awal','nama_akhir'));
    }
}
